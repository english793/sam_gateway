var express = require('express');
var axios = require('axios');
var http = require('http');
var bodyParser = require('body-parser');
var app = express();

const port = 8880;
const samUrl = 'http://127.0.0.1:8888';

var server = http.createServer(app);
server.listen(port, function () {
    console.log("Express 서버가 " + server.address().port + "번 포트에서 Listen중입니다.");
});

//바디 
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());


app.use(function (req, res) {
    let headers = req.headers;
    let requestUrl = '';
    if (req.url.indexOf('?') != -1) {
        requestUrl = req.url.substring(0,req.url.indexOf('?'));    
    } else {
        requestUrl =  req.url;
    }
    
    let bodyData = req.body === undefined ? {} : req.body;
    
    // console.log(headers);
    console.log(req.method);
    console.log(req.body);
    // console.log("query : "+req.query);
    console.log("url : " + samUrl+requestUrl);
    
    axios({
        method: req.method,
        url: samUrl+requestUrl,
        data: bodyData
    }).then(lambdaRes => {
        //console.log(lambdaRes);
        console.log('statusCode : '+lambdaRes.status);
        console.log('statusText : '+lambdaRes.statusText);
        console.log('<data>');
        console.log(lambdaRes.data);
        
        //http에러 체크
        if (lambdaRes.status != 200) {
            res.json({message: "에러 발생", statusCode:lambdaRes.status, statusText:lambdaRes.statusText});
            return false;
        }
        
        //람다 응답 최종 전달하기
        res.json(lambdaRes.data);
        
    }).catch(err =>{
        console.error('<에러>\n ' + err);
        res.json({message: "에러발생"+err.message });
    });
});

